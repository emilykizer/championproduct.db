﻿CREATE TABLE [dbo].[CustomerType] (
    [CustomerTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Code]           VARCHAR (50)   NOT NULL,
    [Name]           VARCHAR (256)  NOT NULL,
    [Description]    VARCHAR (1000) NULL,
    [CreatedDate]    DATETIME       CONSTRAINT [DF_CustomerType_CreatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CustomerType] PRIMARY KEY CLUSTERED ([CustomerTypeId] ASC)
);

