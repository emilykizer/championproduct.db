﻿-- =============================================
-- Author:		Emily Kizer
-- Create date: 4/8/2021
-- Description:	Returns a list of Products.
-- Change Log:

-- SAMPLE CALL:
/*

DECLARE
	@ProductId INT = NULL,
	@IsEnabled BIT = NULL
EXEC [dbo].[spDbo_Product_Get_List]
	@ProductId = @ProductId,
	@IsEnabled = @IsEnabled

*/
-- SELECT * FROM dbo.Product

-- =============================================
CREATE PROCEDURE [dbo].[spDbo_Product_Get_List]
	@ProductId INT = NULL,
	@IsEnabled BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	
		
	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------


	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------


	--------------------------------------------------------------------------------------------
	-- Fetch results.
	--------------------------------------------------------------------------------------------

	SELECT
		ProductId,
		Description,
		CreatedDate,
		IsEnabled
	FROM dbo.Product src
	WHERE (@ProductId IS NULL OR (@ProductId IS NOT NULL AND ProductId = @ProductId))
		AND @IsEnabled IS NULL OR (@IsEnabled IS NOT NULL AND src.IsEnabled = @IsEnabled)



	
	
		
END
