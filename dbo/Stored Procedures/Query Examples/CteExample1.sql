﻿-- Get a list of databases and their ids

/*
;with cte (database_id,[name]) as
   (  
     select 
		database_id,
		[name]
      from sys.databases 
    )  
select 
	database_id,
    [name]
from cte

*/
