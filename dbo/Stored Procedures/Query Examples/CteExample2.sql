﻿-- Get a list of column names by table

/*

;with cte (TableName, ColumnName) as
   (  
     select 
		O.[name] as TableName,
	    C.[name] as ColumnName
       from sysobjects as O
	  inner join syscolumns as C
	        on (O.id = C.id)
	  where O.xtype = 'U' --user tables
    )  
select 
	TableName,
    ColumnName
from cte

*/
